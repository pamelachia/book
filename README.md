# How to use

Url: [http://localhost:5000](http://localhost:5000)

Start up:

```
$ docker-compose build
$ docker-compose up -d
```

Stop:

```
$ docker-compose stop
```

Tear down:

```
$ docker-compose down
```

Force rebuild:

```
$ docker-compose build --no-cache {{container to rebuild}}
```
