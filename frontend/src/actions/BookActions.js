import axios from "axios";
import * as Types from "../types/BookTypes";

const error = error => {
    return {
        type: Types.BOOK_ERROR,
        payload: { error }
    };
};

const success = data => {
    return {
        type: Types.BOOK_SUCCESS,
        payload: { data }
    };
};

export const bookRequest = () => {
    return dispatch => {
        dispatch({
            type: Types.BOOK_REQUEST
        });

        return axios
            .get("api/goodreads")
            .then(res => dispatch(success(res.data)))
            .catch(err => dispatch(error(err.message)));
    };
};
