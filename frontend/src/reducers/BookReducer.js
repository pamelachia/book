import * as Types from "../types/BookTypes";
import Immutable from "immutable";

const defaultState = Immutable.Map({
    loading: false,
    error: false,
    books: Immutable.List()
});

export default (state = defaultState, action) => {
    switch (action.type) {
        case Types.BOOK_REQUEST:
            return state.merge({
                loading: true
            });
        case Types.BOOK_SUCCESS:
            const booksList = state.get("books").push(action.payload.data);
            return state.merge({
                loading: false,
                error: false,
                books: booksList
            });
        case Types.BOOK_ERROR:
            return state.merge({
                loading: false,
                error: true
            });
        default:
            return state;
    }
};
