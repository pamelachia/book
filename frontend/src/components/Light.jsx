import React, { Component } from "react";
import { css, keyframes } from "emotion";

const wobble = keyframes`
    0% {
        left: 40%;
    }
    40% {
        left: 44%;
    }
    50% {
        left: 44%;
    }
    100% {
        left: 40%;
   }
`;
const wrapperStyle = css`
    position: fixed;
    z-index: 200;
    top: 20%;
    right: 0%;
    left: 40%;
    border-radius: 10000px;
    pointer-events: none;
    background: rgba(255, 232, 175, 0.3);

    mix-blend-mode: overlay;
    filter: blur(3vh);

    height: 60vh;
    width: 60vh;
    box-shadow: inset 0px 0px 15vh rgba(255, 232, 175, 0.6),
        0px 0px 10vh rgba(255, 232, 175, 0.6);

    transform: skew(10deg,-20deg);
    outline: 1px solid transparent;

    animation-name: ${wobble};
    animation-duration: 5s;
    animation-iteration-count: infinite;
`;

class Table extends Component {
    render() {
        return <div className={wrapperStyle} />;
    }
}

export default Table;
