import React, { Component } from "react";
import { css } from "emotion";
import tilesImage from "../assets/images/tileable_wood_texture.png";

const wrapperStyle = css`
    position: fixed;
    top: -500%;
    bottom: -100%;
    right: -100%;
    left: -100%;
    filter: blur(2px);
    background-color: #444444;
    background-blend-mode: multiply;

    transform: rotate3d(1, 0, 0, 7deg);
    outline: 1px solid transparent;
    background-image: url(${tilesImage});
`;

class Table extends Component {
    render() {
        return <div className={wrapperStyle} />;
    }
}

export default Table;
