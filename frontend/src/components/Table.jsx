import React, { Component } from "react";
import { css } from "emotion";
import woodImage from "../assets/images/dark_wood.png";

const wrapperStyle = css`
    position: fixed;
    top: 8%;
    bottom: 6%;
    right: -100%;
    left: 0;
    border-radius: 10px;

    box-shadow: inset 0 0 5px rgba(0, 0, 0, 1), inset 0 0 15px rgba(0, 0, 0, 1);

    transform: rotate3d(1, 0, 0, 7deg);
    outline: 1px solid transparent;
    background-image: url(${woodImage});
`;

const bottomStyle = css`
    position: fixed;
    top: 92%;
    bottom: 0;
    right: -100%;
    left: 0;
    background: #000000;
    border-radius: 10px;

    transform: rotate3d(1, 0, 0, 123deg);
    outline: 1px solid transparent;
    background-image: url(${woodImage});
    background-color: #bbbbbb;
    background-blend-mode: multiply;
`;

class Table extends Component {
    render() {
        return (
            <>
                <div className={wrapperStyle} />
                <div className={bottomStyle} />
            </>
        );
    }
}

export default Table;
