import React, { Component } from "react";
import { css, cx } from "emotion";
import { PropTypes } from "prop-types";
import paperImage from "../assets/images/handmadepaper.png";
import { fontFamilies } from "../constants";

const coverStyle = css`
    max-height: 80%;
    max-width: 1024px;
    max-height: 700px;
    width: 80vw;
    height: 60vw;
    display: flex;
    position: relative;

    transform: rotate3d(1, 0, 0, 7deg);
    outline: 1px solid transparent;
`;

const actualCoverStyle = css`
    z-index; 100;
    display: flex;
    position: absolute;
    top: 15px;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: #77100c;
    border-radius: 5px;
    box-shadow: inset 0 0 5px rgba(0, 0, 0, 1),
        inset -5px -5px 5px rgba(0, 0, 0, 0.1),
        0px 10px 10px rgba(0, 0, 0, 0.2),
        10px 10px 10px rgba(0, 0, 0, 0.5);
`;

const actualRightCoverStyle = css`
    left: 53%;
    right: 0;
`;

const bindingStyle = css`
    z-index; 90;
    display: flex;
    position: absolute;
    top: 12px;
    bottom: -2px;
    left: 47%;
    right: 47%;
    background-color: #77100c;
    border-radius: 5px;
    box-shadow: inset 0 0 15px rgba(0, 0, 0, 0.5);
`;

const pagesStyle = css`
    z-index: 200;
    display: flex;
    flex-grow: 1;
    margin: 5px;
`;

const pageStyle = css`
    background-image: url(${paperImage});
    background-color: #aaaaaa;
    background-blend-mode: multiply;
    width: 50%;
    display: flex;
    flex-direction: column;
    box-shadow: 0 0 10px rgba(0, 0, 0, 1);
    border-radius: 2px;
    outline: 1px solid transparent;
    font-family: ${fontFamilies.goudy};
    font-size: 1.1em;
`;

const leftPageStyle = css`
    cursor: w-resize;
    margin-right: 0;
    box-shadow: inset -8px 4px 9px -6px rgba(0, 0, 0, 0.4);
    border-top-right-radius: 15px;
`;

const rightPageStyle = css`
    cursor: e-resize;
    margin-left: 0;
    box-shadow: inset 8px 4px 9px -6px rgba(0, 0, 0, 0.3);
    border-top-left-radius: 15px;
    transform: rotate3d(1, 0, 0, 1deg);
`;

const innerPageStyle = css`
    flex-grow: 1;
    flex-shrink: 1;
    overflow: hidden;
`;

const pageNumberStyle = css`
    margin: 10px auto 30px;
    height: 1em;
`;

const bottomPageStyle = css`
    flex-shrink: 0;
    background-color: rgba(0, 0, 0, 0.4);
    height: 20px;
    box-shadow: inset 0px 0px 20px rgba(0, 0, 0, 0.1);
`;

const bottomLeftPageStyle = css`
    border-top-right-radius: 20px;
`;

const bottomRightPageStyle = css`
    border-top-left-radius: 20px;
`;

class Cover extends Component {
    static propTypes = {
        children: PropTypes.node,
        onPageChange: PropTypes.func.isRequired,
        error: PropTypes.bool
    };

    state = {
        page: 0
    };

    handleChangePage = val => {
        const page = this.state.page + val;
        if (page < 0) {
            return;
        }

        this.setState({ page });
        this.props.onPageChange(page);
    };

    handlePrevPage = () => this.handleChangePage(-1);

    handleNextPage = () => this.handleChangePage(1);

    renderLeftPage() {
        const { children } = this.props;
        const { page } = this.state;

        return (
            <div
                onClick={this.handlePrevPage}
                className={cx(pageStyle, leftPageStyle)}
            >
                <div className={innerPageStyle}>
                    {children[0] ? children[0] : null}
                </div>

                <div className={pageNumberStyle}>{page ? page * 2 : ""}</div>
                <div className={cx(bottomPageStyle, bottomLeftPageStyle)} />
            </div>
        );
    }

    renderRightPage() {
        const { children } = this.props;
        const { page } = this.state;

        return (
            <div
                onClick={this.handleNextPage}
                className={cx(pageStyle, rightPageStyle)}
            >
                <div className={innerPageStyle}>
                    {children[1] ? children[1] : null}
                </div>
                <div className={pageNumberStyle}>{page * 2 + 1}</div>
                <div className={cx(bottomPageStyle, bottomRightPageStyle)} />
            </div>
        );
    }

    render() {
        return (
            <div className={coverStyle}>
                <div className={cx(actualCoverStyle, actualRightCoverStyle)} />
                <div className={actualCoverStyle} />
                <div className={bindingStyle} />

                <div className={pagesStyle}>
                    {this.renderLeftPage()}
                    {this.renderRightPage()}
                </div>
            </div>
        );
    }
}

export default Cover;
