import React, { Component } from "react";
import { PropTypes } from "prop-types";
import Immutable from "immutable";
import Cover from "./Cover";
import BookPage from "./BookPage";
import ReviewPage from "./ReviewPage";

class Book extends Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,
        error: PropTypes.bool.isRequired,
        books: PropTypes.instanceOf(Immutable.List).isRequired,
        onRequestBook: PropTypes.func.isRequired
    };

    state = {
        page: 0
    };

    componentDidMount() {
        this.props.onRequestBook();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { page } = this.state;
        const { books, onRequestBook } = this.props;

        // preload next page
        if (page > prevState.page && page >= books.size) {
            onRequestBook();
        }
    }

    handlePageChange = page => this.setState({ page });

    render() {
        const { books, error } = this.props;
        const { page } = this.state;

        let book;
        if (page !== 0) {
            book = books.get(page - 1);
        }

        return (
            <Cover onPageChange={this.handlePageChange} error={error}>
                <BookPage book={book} />
                <ReviewPage book={book} />
            </Cover>
        );
    }
}

export default Book;
