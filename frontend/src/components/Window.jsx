import React, { Component } from "react";
import { css, keyframes } from "emotion";

const wobble = keyframes`
    0% {
        row-gap: 6%;
        column-gap: 10%;
        filter: blur(1.7vh);
    }
    50% {
        row-gap: 9%;
        column-gap: 12%;
        filter: blur(2vh);
    }
    100% {
        row-gap: 6%;
        column-gap: 10%;
        filter: blur(1.7vh);
   }
`;

const wrapperStyle = css`
    position: fixed;
    z-index: 300;
    pointer-events: none;

    display: grid;
    grid-template-columns: 1fr 1fr;
    column-gap: 12%;
    row-gap: 6%;

    top: 0%;
    left: 10%;

    mix-blend-mode: overlay;
    filter: blur(2vh);

    height: 80vh;
    width: 50vh;
    transform: rotate3d(1, 0.5, -0.5, 20deg);
    outline: 1px solid transparent;

    animation-name: ${wobble};
    animation-duration: 10s;
    animation-iteration-count: infinite;
`;

const squareStyle = css`
    width: 100%;
    height: 100%;
    background: rgba(227, 239, 236, 0.6);
`;

class Window extends Component {
    render() {
        return (
            <div className={wrapperStyle}>
                <div className={squareStyle} />
                <div className={squareStyle} />
                <div className={squareStyle} />
                <div className={squareStyle} />
            </div>
        );
    }
}

export default Window;
