import React, { Component } from "react";
import { css } from "emotion";
import { PropTypes } from "prop-types";

const wrapperStyle = css`
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
`;

const imageStyle = css`
    filter: grayscale(100%);
    mix-blend-mode: multiply;
    margin: 10% auto;
    display: block;
    flex-shrink: 0;
    height: 40%;

    :hover {
        filter: none;
        opacity: 1;
    }
`;

const titleStyle = css`
    margin: 30px auto;
    max-width: 60%;
    text-align: center;
    flex-grow: 1;
`;

const BookImage = ({ bookDetail }) => {
    let imageUrl = bookDetail.image_url;

    if (imageUrl.indexOf("nophoto") < 0) {
        var pos = imageUrl.lastIndexOf("m");
        if (pos > 0) {
            imageUrl = `${imageUrl.substring(0, pos)}l${imageUrl.substring(
                pos + 1
            )}`;
        }
    }

    return <img className={imageStyle} src={imageUrl} alt="book-cover" />;
};

const BookTitle = ({ bookDetail }) => (
    <div className={titleStyle}>{bookDetail.title}</div>
);

const BookAuthor = ({ bookDetail }) => {
    const author = bookDetail.authors.author;
    if (!author) {
        return null;
    }

    return <div className={titleStyle}>By {author.name}</div>;
};

class ReviewPage extends Component {
    static propTypes = {
        book: PropTypes.object
    };

    render() {
        const { book } = this.props;

        if (!book) return null;

        const bookDetail = book.book;

        return (
            <div className={wrapperStyle}>
                <BookImage bookDetail={bookDetail} />
                <BookTitle bookDetail={bookDetail} />
                <BookAuthor bookDetail={bookDetail} />
            </div>
        );
    }
}

export default ReviewPage;
