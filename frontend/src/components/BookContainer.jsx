import React, { Component } from "react";
import { connect } from "react-redux";
import { bookRequest } from "../actions/BookActions";
import Book from "./Book";
import { PropTypes } from "prop-types";
import Immutable from "immutable";

const mapStateToProps = state => {
    return { BookStore: state.BookReducer };
};

const mapDispatchToProps = {
    bookRequest
};

class BookContainer extends Component {
    static propTypes = {
        BookStore: PropTypes.instanceOf(Immutable.Map).isRequired,
        bookRequest: PropTypes.func.isRequired
    };

    handleRequestBook = () => {
        this.props.bookRequest();
    };

    render() {
        const { BookStore } = this.props;

        return (
            <Book
                loading={BookStore.get("loading")}
                error={BookStore.get("error")}
                books={BookStore.get("books")}
                onRequestBook={this.handleRequestBook}
            />
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BookContainer);
