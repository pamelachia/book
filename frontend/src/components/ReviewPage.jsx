import React, { Component } from "react";
import { css } from "emotion";
import { PropTypes } from "prop-types";
import goodreadsImage from "../assets/images/goodreads_logo.png";

const wrapperStyle = css`
    display: flex;
    flex-direction: column;
    height: 100%;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    line-height: 2em;
`;

const goodreadsUrlStyles = css`
    display: block;
    margin-top: 10%;
`;

const goodreadsStyles = css`
    filter: grayscale(100%);
    opacity: 0.7;
    mix-blend-mode: multiply;
    height: 40px;
    width: 40px;
    display: block;

    :hover {
        filter: none;
    }
`;

const RatingStars = ({ rating }) => {
    return Array.from(Array(5), (_, i) =>
        i < rating ? <span key={i}>&#9733;</span> : <span key={i}>&#9734;</span>
    );
};

const RatingOpinion = ({ rating }) => {
    rating = parseInt(rating);
    if (rating === 1) {
        return (
            <span>
                I wouldn't touch it, but that's just my opinion... right?
            </span>
        );
    }
    if (rating === 2) {
        return <span>It's a book.</span>;
    }
    if (rating === 3) {
        return <span>It's chill.</span>;
    }
    if (rating === 4) {
        return <span>You should give it a try.</span>;
    }
    if (rating === 5) {
        return <span>An absolute must.</span>;
    }
};

class ReviewPage extends Component {
    static propTypes = {
        book: PropTypes.object
    };

    render() {
        const { book } = this.props;

        if (!book) return null;

        return (
            <div className={wrapperStyle}>
                <div>
                    <RatingStars rating={book.rating} />
                </div>
                <div>
                    <RatingOpinion rating={book.rating} />
                </div>
                <a
                    className={goodreadsUrlStyles}
                    href={book.book.link}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <img
                        className={goodreadsStyles}
                        src={goodreadsImage}
                        alt="goodreads-logo"
                    />
                </a>
            </div>
        );
    }
}

export default ReviewPage;
