import React, { Component } from "react";
import { css } from "emotion";
import BookContainer from "./components/BookContainer";
import Floor from "./components/Floor";
import Table from "./components/Table";
import Light from "./components/Light";
import Window from "./components/Window";

const wrapperStyle = css`
    height: 100vh;
    background-color: #352601;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    perspective: 1000px;
`;

class App extends Component {
    render() {
        return (
            <div className={wrapperStyle}>
                <Floor />
                <Table />
                <Light />
                <Window />
                <BookContainer />
            </div>
        );
    }
}

export default App;
