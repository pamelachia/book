FROM tiangolo/uvicorn-gunicorn-fastapi

COPY ./requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt
