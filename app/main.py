import sys
import os
import pickle
import requests
import random

import redis
import xmltodict
from fastapi import FastAPI

version = f"{sys.version_info.major}.{sys.version_info.minor}"

app = FastAPI()

user_id = 31793449


def request_books_page(shelf, user_id, page):
    req = requests.get(
        "https://www.goodreads.com/review/list.xml",
        {
            "v": "2",
            "key": os.environ["GOODREADS_API_KEY"],
            "id": user_id,
            'shelf': shelf,
            'per_page': 200,
            'page': page
        },
    )

    if req.status_code != 200:
        return None

    parsed = xmltodict.parse(req.content)
    reviews = parsed["GoodreadsResponse"]["reviews"]
    if "review" not in reviews:
        return []

    return reviews["review"]


def request_books():
    page = 1
    books = []
    last_result = []
    while page == 1 or len(last_result) > 0:
        last_result = request_books_page("read", user_id, page)
        if last_result is None:
            return None

        books += last_result
        page += 1

    return books


def get_books():
    r = redis.Redis(host='redis', port=6379, db=0)

    books = r.get("books")
    if not books:
        books = request_books()
        if books is None:
            return None

        r.setex("books", 9000, pickle.dumps(books))
    else:
        books = pickle.loads(books)

    return books


@app.get("/api/goodreads")
async def read_goodreads():
    books = get_books()

    if books is None:
        return {"error": "Couldn't fetch books"}

    best_books = list(filter(lambda book: book['rating'] in ("4", "5"), books))
    return random.choice(best_books)
